const getSum = (str1, str2) => {
  if (
    typeof str1 !== "string" ||
    typeof str2 !== "string" ||
    Number.isNaN(Number(str1)) ||
    Number.isNaN(Number(str2))
  ) {
    return false;
  }

  if (str1.length === 0) {
    str1 = 0;
  }
  if (str2.length === 0) {
    str2 = 0;
  }
  return parseInt(str1, 10) + parseInt(str2, 10) + "";
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = listOfPosts.filter((post) => post.author === authorName);
  let comments = listOfPosts.flatMap((post) => {
    if (post.comments) {
      return post.comments.filter((comment) => comment.author === authorName);
    } else {
      return [];
    }
  });

  return `Post:${posts.length},comments:${comments.length}`;
};

const tickets = (people) => {
  let cash = 0;
  let ticket = 25;
  let peopleNum = people.map((el) => parseInt(el, 10));
  for (let customer of peopleNum) {
    if (customer === ticket) {
      cash += customer;
    } else if (customer - ticket - cash > 0) {
      return "NO";
    } else {
      cash += ticket;
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
